defmodule Echo do
  
  # start
  
  def start() do
    pid = spawn(&Echo.echoServer/0)
    Process.register(pid, :echo)
    :ok
  end
  
  # stop
  
  def stop() do
    send(Process.whereis(:echo), {:stop, :kill})
    Process.unregister(:echo)
    :ok
  end
  
  # print
  
  def print(term) do
    send(Process.whereis(:echo), {:print, term})
    :ok
  end
  
  def echoServer() do 
    receive do
      {:print, term} -> 
        IO.puts term
        echoServer()
      {:exit, reason} -> 
        reason
        # sair
    end 
  end
  
end


