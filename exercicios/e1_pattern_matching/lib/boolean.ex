defmodule Boolean do

  # b_not
  
  def b_not(false) do
	true
  end
  def b_not(true) do
	false
  end
  
  # b_and
  
  def b_and(true, true) do
	true
  end
  def b_and(true, false) do
	false
  end
  def b_and(false, true) do
	false
  end
  def b_and(false, false) do
	false
  end
  
  # b_or
  
  def b_or(true, true) do
	true
  end
  def b_or(true, false) do
	true
  end
  def b_or(false, true) do
	true
  end
  def b_or(false, false) do
	false
  end
  
end
