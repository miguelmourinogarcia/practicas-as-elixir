defmodule Mi6 do
  require GenServer
  require Db
  require Agent
  require Create
  require Manipulating

  # Server
  # Callbacks
  
  
  
  

  # FUNDAR
  
  def init(state) do
    {:ok, state}
  end



  # RECRUTAR
  
  def handle_cast({:recrutar, [axente, destino]}, state) do
  
    {result, _} = Db.read(state, axente)
    if result == :error do

      list = String.length(destino) |> Create.create |> Enum.shuffle()
      {:ok, agentPid} = Agent.start_link(fn -> list end)
    
      {:noreply, Db.write(state, axente, agentPid)}
    else
      {:noreply, state}
    end
  end
  
  
  
  # ASIGNAR_MISION
  
  def handle_cast({:asignar_mision, [axente, mision]}, state) when mision == :contrainformar do
    
    {result, agentPid} = Db.read(state, axente)
    if result != :error do
      Agent.update(agentPid, fn (stateAgent) -> Manipulating.reverse(stateAgent) end)
    end
    
    {:noreply, state}

  end
  
  def handle_cast({:asignar_mision, [axente, mision]}, state) when mision == :espiar do
    
    {result, agentPid} = Db.read(state, axente)
    if result != :error do
      Agent.update(agentPid, fn (stateAgent) -> Manipulating.filter(stateAgent, hd(stateAgent)) end)
    end
    
    {:noreply, state}
  end

  
  
  # CONSULTAR_ESTADO
  
  def handle_call({:consultar_estado, [axente]}, _from, state) do
  
    {result, agentPid} = Db.read(state, axente)
    if result == :error do
      {:reply, :you_are_here_we_are_not, state}
    else
      list = Agent.get(agentPid, fn stateAgent -> stateAgent end)
      {:reply, list, state}
    end

  end
  
  
  
  # DISOLVER
  
  def terminate(reason, state) do
    IO.puts reason
    Db.destroy(state)
  end

  
  # API
  
  def fundar() do
    {_, _} = GenServer.start_link(Mi6, Db.new(), name: :mi6)
    :ok
  end
  
  def recrutar(axente, destino) do
    GenServer.cast(:mi6, {:recrutar, [axente, destino]})
  end
  
  def asignar_mision(axente, mision) do
    GenServer.cast(:mi6, {:asignar_mision, [axente, mision]})
  end
  
  def consultar_estado(axente) do
    GenServer.call(:mi6, {:consultar_estado, [axente]})
  end

  def disolver() do
    GenServer.stop(:mi6, :normal)
  end
  
end
