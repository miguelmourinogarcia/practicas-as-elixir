defmodule Sorting do

  # mergesort

  def mergesort([_single] = list) do
	list
  end
  
  def mergesort([] = list) do
	list
  end
  
  def mergesort(list) do
    {left, right} = Enum.split(list, div(length(list), 2))
    :lists.merge(mergesort(left), mergesort(right))
  end

  # quicksort
  
  def quicksort([_single] = list) do
	list
  end

  def quicksort([] = list) do
	list
  end
  
  def quicksort([head | tail]) do
    {left, right} =  Enum.split_with(tail, fn x -> x < head end)
    quicksort(left) ++ [head] ++ quicksort(right)
  end
  
end
