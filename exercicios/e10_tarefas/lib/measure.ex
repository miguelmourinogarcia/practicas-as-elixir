defmodule Measure do
  require Manipulating
  require Sorting

  def run(list, num) do
  
    # lista cos procesos encargados da creacion das listas
    listOfTasks = creationTasks(list, num, [])
    
    # esperase a que acaben todos os procesos de creacion, e midese o tempo
    a = :erlang.timestamp()
    listOfListsOfData = awaitCreationTasks(listOfTasks, [])
    b = :erlang.timestamp()
    tCreacion = :timer.now_diff(b, a) / (1000 * 1000)

    # lista cos procesos encargados do procesamento das listas
    listOfTasksEx = execTasks(list, listOfListsOfData, [])
    #devolven unha lista cos tempos respectivos, referentes a canto tardaron en executarse as tarefas
    listOfTimes = awaitExecTask(listOfTasksEx, [])
    
    # impresion pantalla dos resultados
    IO.puts "------------------------------------------------------"
    IO.puts "| Creación de datos \t\t: #{tCreacion} \t sec |"
    impResultado(list, listOfTimes)

  end
  
  
  # funcion de impresion 
  
  defp impResultado([headFn | tailFn], [headTimes | tailTimes]) when headTimes == "interrompido" do
    {module, fun} = headFn
    IO.puts "| #{module}:#{fun} \t: #{headTimes} \t     |"
    impResultado(tailFn, tailTimes)
  end
  
  defp impResultado([headFn | tailFn], [headTimes | tailTimes]) when headTimes != "interrompido" do
    {module, fun} = headFn
    IO.puts "| #{module}:#{fun} \t: #{headTimes} \t sec |"
    impResultado(tailFn, tailTimes)
  end
  
  defp impResultado([], []) do
    IO.puts "------------------------------------------------------"
  end
  
  
  # funcion de creacion de tarefas de execuxion
  
  defp execTasks([headFn | tailFn], [headArg | tailArg], taskExList) do
  
    {module, fun} = headFn
    task = Task.async(fn -> apply(module, fun, [headArg]) end)
    taskExList = taskExList ++ [task]
    
    execTasks(tailFn, tailArg, taskExList)

  end
  
  defp execTasks([], [], taskExList) do
      taskExList
  end
  
  
  # funcion de creacion de tarefas de creacion de datos
  
  defp creationTasks([headFn | tailFn], num, creationLists) when headFn == {Manipulating, :flatten} do
  
    task = Task.async(fn -> Measure.creationListOfLists(num) end)
    creationLists = creationLists ++ [task]

    creationTasks(tailFn, num, creationLists)
    
  end
  
  defp creationTasks([_ | tailFn], num, creationLists) do
  
    task = Task.async(fn -> Measure.creationList(num) end)
    creationLists = creationLists ++ [task]

    creationTasks(tailFn, num, creationLists)
    
  end
  
  defp creationTasks([], _, creationLists) do
      creationLists
  end
  
  
  # funcion de espera de tarefas de creacion, que devolve o resultado destas
 
  defp awaitCreationTasks([head | tail], listOfLists) do
    listOfLists = listOfLists ++ [Task.await(head, :infinity)]
    awaitCreationTasks(tail, listOfLists)
  end
  
  defp awaitCreationTasks([], listOfLists) do
    listOfLists
  end
  
  
  # funcion de espera de tarefas de execucion, devolve os tempos consumidos por cada tarefa
  
  defp awaitExecTask([head | tail], listOfTimes) do
  
    # intentase esperar 10 segundos, se non, o tempo marcase como "interrompido"
    try do
	  a = :erlang.timestamp()
	  Task.await(head, 10000)
	  b = :erlang.timestamp()
	  tExec = :timer.now_diff(b, a) / (1000 * 1000)
	  awaitExecTask(tail, listOfTimes ++ [tExec])
    catch
      :exit, _ -> awaitExecTask(tail, listOfTimes ++ ["interrompido"])
    end
  end
  
  defp awaitExecTask([], listOfTimes) do
    listOfTimes
  end
  
 
  # funcions de creacion das listas de datos
  
  def creationList(num) do
    1..num |> Enum.to_list() |> Enum.map(fn _ -> (:rand.uniform(1001)-1) end)
  end
  
  def creationListOfLists(num) do
    list = creationList(num)
    Enum.map(list, fn _ -> list end)
  end
  
  
end


