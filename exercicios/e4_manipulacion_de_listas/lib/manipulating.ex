defmodule Manipulating do

  # filter
  
  def filter(list, n) do
	filter_aux(list, n, [])
  end
  
  defp filter_aux([head | tail], n, resultList) when head <= n do
	filter_aux(tail, n, [head | resultList])
  end
  
  defp filter_aux([head | tail], n, resultList) when head > n do
	filter_aux(tail, n, resultList)
  end
  
  defp filter_aux([], _, resultList) do
	reverse(resultList)
  end
  
  # reverse
  
  def reverse(list) do
	reverse_aux(list, [])
  end
  
  defp reverse_aux([head | tail], resultList) do
	reverse_aux(tail, [head | resultList])
  end
  
  defp reverse_aux([], resultList) do
	resultList
  end
  
  # concatenate
  
  def concatenate([]) do
	[]
  end
  
  def concatenate([head | tail]) do
	concatenate_aux(head, tail, [])
  end
  
  defp concatenate_aux([head | tail], restList, resultList) do
	concatenate_aux(tail, restList, [head | resultList])
  end
  
  defp concatenate_aux([], [head | tail], resultList) do
	concatenate_aux(head, tail, resultList)
  end
  
  defp concatenate_aux([], [], resultList) do
	reverse(resultList)
  end
  
  # flatten
  
  def flatten([]) do
    []
  end
  
  def flatten([head | tail]) do
    concatenate [flatten(head), flatten(tail)]
  end 
  
  def flatten(head) do
    [head]
  end
  
end
