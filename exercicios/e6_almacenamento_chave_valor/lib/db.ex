defmodule Db do


  # new 
  
  def new() do
    []
  end
  
  
  # write
  
  def write(db_ref, key, element) do
    [{key, element} | db_ref]
  end
  
  
  # delete
  
  def delete(db_ref, key) do
    delete_aux(db_ref, key, [])
  end
  
  defp delete_aux([{stored_key, _} | tail], key, new_db_ref) when stored_key == key do
    new_db_ref ++ tail
  end
  
  defp delete_aux([{stored_key, element} | tail], key, new_db_ref) when stored_key != key do
    delete_aux(tail, key, [{stored_key, element} | new_db_ref])
  end
  
  defp delete_aux([], _, new_db_ref) do
    new_db_ref
  end


  # read 
  
  def read([{stored_key, element} | _], key) when stored_key == key do
      {:ok, element}
  end
  
  def read([{stored_key, _} | tail], key) when stored_key != key do
      read(tail, key)
  end
  
  def read([], _) do
    {:error, :not_found}
  end
  
  
  # match
  
  def match(db_ref, element) do
    match_aux(db_ref, element, [])
  end
  
  defp match_aux([{key, stored_element} | tail], element, keys) when stored_element == element do
      match_aux(tail, element, [key | keys])
  end
  
  defp match_aux([{_, stored_element} | tail], element, keys) when stored_element != element do
      match_aux(tail, element, keys)
  end
  
  defp match_aux([], _, keys) do
    keys
  end
  
  
  # destroy
  
  def destroy(_) do
    :ok
  end
  
  
end
