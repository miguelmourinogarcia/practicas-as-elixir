defmodule Create do

  # create
  
  def create(n) do
	create_aux([], 0, n)
  end
  
  defp create_aux(list, n, max) when n == max do
	list
  end
  
  defp create_aux(list, n, max) when n < max do
	create_aux(list ++ [n + 1], n + 1, max)
  end
  
  # reverse_create 

  def reverse_create(n) do
	reverse_create_aux([], n)
  end
  
  defp reverse_create_aux(list, n) when  n == 0 do
	list
  end
  
  defp reverse_create_aux(list, n) when  n > 0 do
	reverse_create_aux(list ++ [n], n - 1)
  end
  
end
