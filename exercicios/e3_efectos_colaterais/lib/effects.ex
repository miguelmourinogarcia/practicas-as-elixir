defmodule Effects do
  require Integer
  
  # print

  def print(n) do
	print_aux(1, n + 1)
  end
  
  defp print_aux(n, max) when n < max do
	IO.puts n
	print_aux(n + 1, max)
  end
  
  defp print_aux(n, max) when n >= max do
  end
  
  # even_print
  
  def even_print(n) do
	even_print_aux(1, n + 1)
  end
  
  defp even_print_aux(n, max) when n < max do
	if Integer.is_even(n) do
	  IO.puts n
	end
	even_print_aux(n + 1, max)
  end
  
  defp even_print_aux(n, max) when n >= max do
  end
  
end
