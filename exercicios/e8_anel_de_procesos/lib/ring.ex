defmodule Ring do

  def start(n, m, msg) do
  
    # Creacion de lista con los procesos del anillo
    list = createProcs(m, msg, [], n)
    [head | _] = list
    
    # Comienzo del envio (estrategia proceso distinguido), 
    # empezamos por la cabeza y enviamos lista de procesos e iteraciones iniciales
    send head, {{Enum.reverse(list), 1}, msg}
    
    # Comprobacion de salida de los procesos
    terminar list
    
  end
  
  
  defp createProcs(m, msg, list, rest) when rest > 0 do

    # Creacion de proceso, funcion loop encargada de iterar
    {proc, _} = spawn_monitor(Ring, :loop, [m])
    list = [proc | list]
    createProcs(m, msg, list, rest - 1)
    
  end
  
  defp createProcs(_, _, list, rest) when rest == 0 do
    list  
  end

  defp terminar(procs) do
  
    IO.puts "Esperando pids #{inspect procs}"
    receive do
    
      # Si encontramos atom DOWN en el buzon de msgs...
      {:DOWN, _, _, proc, _} ->
        IO.puts "#{inspect proc} terminado"
        procs = List.delete(procs, proc)
        
        # Estrategia recursiva
        unless Enum.empty?(procs) do
          terminar(procs)
      end
    end
    
  end
  
  def loop(m) do
  
    receive do
       
      # fin, enviamos fin y siguen saliendo los procesos
      {{[next | rest], contador}, :fin} when contador == m -> 
        send next, {{rest, contador}, :fin}

      # m = contador, se alcanzaron los mensajes, se vacia la lista de procesos y se envia fin
      {{[next | rest], contador}, msg} when contador == m -> 
        impIO(msg, contador, self())
        send next, {{rest, contador}, :fin}

      # m < contador, se sigue iterando enlazando el proceso que envia a la cola de la lista de procs
      {{[next | rest], contador}, msg} when contador < m -> 
        impIO(msg, contador, self())
        send next, {{rest ++ [next], contador+1}, msg}
        loop(m)
        
    end
    
  end
  
  defp impIO(msg, contador, pid) do
  
    IO.puts "Recibido: #{inspect msg} numero #{inspect contador} #{inspect pid}"
  
  end
  
end
